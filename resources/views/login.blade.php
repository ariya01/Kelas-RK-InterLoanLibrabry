<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bootstrap Material Admin by Bootstrapious.com</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
  <!-- Fontastic Custom icon font-->
  <link rel="stylesheet" href="css/fontastic.css">
  <!-- Google fonts - Poppins -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
      </head>
      <body>
        <div class="page login-page">
          <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
              <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-6">
                  <div class="info d-flex align-items-center">
                    <div class="content">
                      <div class="logo">
                        <h1>InterLibrary Loan ITS</h1>
                      </div>
                      <p>Sistem informasi perpustakaan yang terintegrasi</p>
                    </div>
                  </div>
                </div>
                <!-- Form Panel    -->
                <div class="col-lg-6 bg-white">
                  <div class="form d-flex align-items-center">
                    <div class="content">
                      <form method="post" class="form-validate" action="{{ URL('login_akun') }}">
                        {{csrf_field()}}
                        <div class="form-group ">
                         <div class="dropdown">
                          <div class="form-group">
                            <select id="Status" name="account" class="btn btn-primary">
                              <option value="0">Pilih Login</option>
                              <option value="1">Mahasiswa</option>
                              <option value="2">Dosen</option>
                              <option value="3">Karyawan/Tendik</option>
                              <option value="4">Umum</option>
                              <option value="5">Pustakawan</option>
                            </select>
                          </div>
                        </div>            
                      </div>
                      <div class="form-group">
                        <input id="login-username" type="text" name="nama" id="nama" required data-msg="Please enter your username" class="input-material">
                        <label for="login-username" id="ganti" class="label-material">Pilih Role</label>
                      </div>
                      <div class="form-group">
                        <input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                        <label for="login-password" class="label-material">Password</label>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Login</button>
                        <a href="#" class="forgot-pass">*Login dengan akun integra ITS</a><br><small>
                        </div>
                        <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="copyrights text-center">
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
          <!-- JavaScript files-->
          <script src="vendor/jquery/jquery.min.js"></script>
          <script src="vendor/popper.js/umd/popper.min.js"> </script>
          <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
          <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
          <script src="vendor/chart.js/Chart.min.js"></script>
          <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
          <!-- Main File-->
          <script src="js/front.js"></script>
          <script type="text/javascript">
            $("#Status").on('change', function() {
              // alert($(this).val());
              if ($(this).val() == '1')
              {
                $('#ganti').text('No. KTM');
              } 
              else if ($(this).val() == '4') 
              {
                $('#ganti').text('No. KTP');
              }
              else
              {
                $('#ganti').text('No. NIP'); 
              }
            });
          </script>
        </body>
        </html>