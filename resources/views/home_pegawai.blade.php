@extends('master')
@section('slide1')
active
@endsection
@section('content')
<div class="row bg-white has-shadow">
	<!-- Item -->
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-padnote"></i></div>
			<div class="title"><span><br>Makalah</span>
				<div class="progress">
					<div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
				</div>
			</div>
			<div class="number"><strong>70</strong></div>
		</div>
	</div>
	<!-- Item -->
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-bill"></i></div>
			<div class="title"><span>Buku<br>Tugas Akhir</span>
				<div class="progress">
					<div role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
				</div>
			</div>
			<div class="number"><strong>40</strong></div>
		</div>
	</div>
	<!-- Item -->
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-check"></i></div>
			<div class="title"><span>Buku<br>Text</span>
				<div class="progress">
					<div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
				</div>
			</div>
			<div class="number"><strong>50</strong></div>
		</div>
	</div>
	<!-- Item -->
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-user"></i></div>
			<div class="title"><span><br>Paper</span>
				<div class="progress">
					<div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
				</div>
			</div>
			<div class="number"><strong>25</strong></div>
		</div>
	</div>
</div>
<br>
<div class="row bg-white has-shadow">
<div class="col-xl-3 col-sm-6">
		
	</div>
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-paper-airplane"></i></div>
			<div class="title"><span>Buku<br>Terpinajm</span>
				<div class="progress">
					<div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
				</div>
			</div>
			<div class="number"><strong>50</strong></div>
		</div>
	</div>
	<!-- Item -->
	<div class="col-xl-3 col-sm-6">
		<div class="item d-flex align-items-center">
			<div class="icon bg-violet"><i class="icon-clock"></i></div>
			<div class="title"><span>Belum <br> Kembali</span>
				<div class="progress">
					<div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
				</div>
			</div>
			<div class="number"><strong>25</strong></div>
		</div>
	</div>
</div>

@endsection
