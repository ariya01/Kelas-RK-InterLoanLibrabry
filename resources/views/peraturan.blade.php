@extends('master')
@section('slide10')
active
@endsection
@section('content')
			<div class="container">
				<div class="row" style="padding-bottom: 5px;padding-top: 5px;">
					<div class="col-lg-12">
					<div class="card">
						<div class="card-close">
	                      <div class="dropdown">
	                        <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
	                        <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
	                      </div>
	                    </div>
	                    <div class="card-header d-flex align-items-center">
	                      <h3 class="h4">Daftar Jenis Buku Boleh Dipinjam</h3>
	                    </div>
	                    <div class="card-body">
	                    	<div>
	                    		<button data-toggle="modal" data-target="#myModal" class="btn btn-primary" style="padding-top: 3px;">Edit Aturan Buku</button>
	                    	</div>
	                    		 <div class="table-responsive" style="padding-top: 5px;">
			                        <table class="table">
			                          <thead>
			                            <tr>
			                              <th>No</th>
			                              <th>Jenis Buku</th>
			                              <th class="text-center">Status</th>
			                              <th class="text-center">Option</th>
			                            </tr>
			                          </thead>
			                          <tbody>
			                            <tr>
			                              <th scope="row">1</th>
			                              <td>Text Book</td>
			                              <td class="text-center">
			                              	<button class="btn btn-success">Boleh</button>
			                              </td>
			                              <td class="text-center">
			                                <button class="btn btn-danger">Hapus</button>
			                              </td>
			                            </tr>
                                  <tr>
                                    <th scope="row">2</th>
                                    <td>Tuga Akhir</td>
                                    <td class="text-center">
                                      <button class="btn btn-danger">Tidak</button>
                                    </td>
                                    <td class="text-center">
                                      <button class="btn btn-danger">Hapus</button>
                                    </td>
                                  </tr>
			                          </tbody>
			                        </table>
                      			</div>
	                    </div>
				</div>
				</div>
				</div>
				
				
			</div>
			<div class="container">
              <div class="row" style="padding-top: 5px;">
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Peraturan Perpustakaan</h3>
                    </div>
                    <div class="card-body">
                      <form>
                        <div class="form-group">
                          <label>Nama RBTC</label>
                          <div>
                            <input type="text" class="form-control" value="Informatika" disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group">
                          <label class="">Lama Peminjaman</label>
                          		<input type="number" name="tersedia" class="form-control" placeholder="Berapa Hari" >
                        </div>
                        <div class="form-group">
                          <label class="">Penetapan Denda</label>
                          		<input type="text" name="denda" class="form-control" placeholder="Rp.5000">
                        </div>
                        <div class="line"></div>
                        <div class="form-group">
                            {{-- <button type="submit" class="btn btn-secondary">Cancel</button> --}}
                            <a href="{{ URL('pinjam') }}" class="btn btn-primary">Setuju</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <!-- Form Elements -->
              </div>
            </div>

            {{-- Modal --}}
            <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">Boleh Dipinjam</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                               <form>
                        <div class="form-group">
                          <label class="">Nama RBTC</label>
                          <div class="">
                            <input type="text" class="form-control" value="Informatika" disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group">
                          <label class="">Buku Yang Boleh Dipinjam</label>
                          	<div class="">
                          		<select class="form-control" name="jenis_buku">
						              <option value="1">Text Book</option>
						              <option value="2">Jurnal</option>
						              <option value="3">Makalah</option>
						              <option value="4">Tugas Akhir</option>	
            					</select>
                          	</div>
                        </div>
                        <div class="line"></div>
                        <div class="modal-footer">
                          <div class="">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancel</button>
                            <a href="{{ URL('pinjam') }}" class="btn btn-primary">Setuju</a>
                          </div>
                        </div>
                      </form>
             </div>
              
@endsection