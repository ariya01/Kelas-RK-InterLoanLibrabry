@extends('master')
@section('slide6')
active
@endsection
@section('content')

<div class="row">
  <div class="col-lg-4">                           
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Pilih Jurusan</h3>
      </div>
      <div class="card-body">
        <form class="form-inline">
          <div class="form-group text-center">
            <select id="inlineFormInput" name="account" class="btn btn-primary">
              <option>Pilih Jurusan</option>
              <option>Informatika</option>
              <option>Matematika</option>
              <option>Statistika</option>
            </select>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Inline Form-->
  <div class="col-lg-8">                           
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Cari Buku</h3>
      </div>
      <div class="card-body">
        <form class="form-inline">
          <div class="form-group">
            <label for="inlineFormInput" class="sr-only">Name</label>
            <input id="inlineFormInput" type="text" placeholder="Judul Buku" class="mr-3 form-control">
          </div>
          <div class="form-group">
            <label for="inlineFormInputGroup" class="sr-only">Username</label>
            <input id="inlineFormInputGroup" type="text" placeholder="Penerbit" class="mr-3 form-control">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Cari</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-4">                           
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Tambah Buku</h3>
      </div>
      <div class="card-body">
       <button data-toggle="modal" data-target="#myModal1" class="btn btn-primary">Tambah</button>
     </div>
   </div>
 </div>
 <div class="col-lg-12">
  <div class="card">
    <div class="card-close">
      <div class="dropdown">
        <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
        <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
      </div>
    </div>
    <div class="card-header d-flex align-items-center">
      <h3 class="h4">Daftar Buku</h3>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <!-- <th>No</th> -->
              <th>Cover</th>
              <th>Nama Buku</th>
              <th>Penerbit</th>
              <th>Kode Buku</th>
              <th>Jenis Buku</th>
              <th>Jurusan</th>
              <th>Tersedia</th>
              <th class="text-center" width="30%;">Option</th>
            </tr>

          </thead>
          <tbody>
           @foreach ($data as $data)
           <tr>
            <!-- <th scope="row">1</th> -->
            <th><a href=""><img src="{{asset('/images/'.$data->gambar)}}" height="100"></a></th>
            <td>{{$data->nama_buku}}</td>
            <td>{{$data->penerbit}}</td>
            <td>{{$data->kode_buku}}</td>
            <td>{{$data->jenis_buku}}</td>
            <td>{{$data->jurusan}}</td>
            <td>{{$data->tersedia}}</td>
            <td class="text-center">
              <div class="row text-center" style="margin-top: -15%; margin-left: 20%;">
                <button data-toggle="modal" onclick="reply_click(this.value)" id="hai" value="{{$data->id_buku}}" data-target="#myModal" class="btn btn-primary">Edit</button>
                <form method="post" action="{{url('hapusbuku')}}" style="margin-left: 4%;">
                  <input type="hidden" name="id_buku" value="{{$data->id_buku}}">
                  {{ csrf_field() }}
                  <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<!-- Modal Form-->
<div id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">TAMBAH BUKU</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{url('inputbuku')}}" enctype="multipart/form-data">
          <div class="form-group">
            <label>Nama Buku</label>
            <input type="text" name="nama_buku" class="form-control" >
          </div>
          <div class="form-group">
            <label>Gambar</label>
            <input type="file" name="gambar" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Penerbit</label>
            <select class="form-control" name="penerbit">
              <option value="1">ITS</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kode Buku</label>
            <input type="text" name="kode_buku" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Jenis Buku</label>
            <select class="form-control" name="jenis_buku">
              <option value="1">Text Book</option>
              <option value="2">Jurnal</option>
              <option value="3">Makalah</option>
              <option value="4">Tugas Akhir</option>
            </select>
          </div>
          <div class="form-group">       
            <label>Lokasi</label>
            <select class="form-control" name="jurusan">
              <option value="1">Informatika</option>
            </select>
          </div>
          <div class="form-group">       
            <label>Jumlah</label>
            <input type="number" name="tersedia" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Detail</label>
            <input type="text" name="deskripsi" class="form-control" >
          </div>
          {{ csrf_field() }}
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">EDIT BUKU</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
       <form method="post" action="{{url('editbuku')}}" enctype="multipart/form-data">
          <div class="form-group">
            <label>Nama Buku</label>
            <input type="text" name="nama_buku" class="form-control" >
          </div>
          <div class="form-group">
            <label>Gambar</label>
            <input type="file" name="gambar" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Penerbit</label>
            <select class="form-control" name="penerbit">
              <option value="1">ITS</option>
            </select>
          </div>
          <div class="form-group">
            <label>Kode Buku</label>
            <input type="text" name="kode_buku" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Jenis Buku</label>
            <select class="form-control" name="jenis_buku">
              <option value="1">Text Book</option>
              <option value="2">Jurnal</option>
              <option value="3">Makalah</option>
              <option value="4">Tugas Akhir</option>
            </select>
          </div>
          <div class="form-group">       
            <label>Lokasi</label>
            <select class="form-control" name="jurusan">
              <option value="1">Informatika</option>
            </select>
          </div>
          <div class="form-group">       
            <label>Jumlah</label>
            <input type="number" name="tersedia" class="form-control" >
          </div>
          <div class="form-group">       
            <label>Detail</label>
            <input type="text" name="deskripsi" class="form-control" >
          </div>
          <input type="hidden" name="id_buku" id="editbuku">
          {{ csrf_field() }}
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
            <button type="submit" class="btn btn-primary">Edit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  function reply_click(clicked_id)
  {
    document.getElementById('editbuku').value = clicked_id;
  }
</script>
@endsection
