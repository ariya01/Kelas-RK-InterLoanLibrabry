@extends('master_user')
@section('slide4')
active
@endsection
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<div class="row">
  <div class="col-lg-4">                           
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Pilih Jurusan</h3>
      </div>
      <div class="card-body">
        <form class="form-inline">
          <div class="form-group text-center">
            <select id="inlineFormInput" name="account" class="btn btn-primary">
              <option>Pilih Jurusan</option>
              <option>Informatika</option>
              <option>Matematika</option>
              <option>Statistika</option>
            </select>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Inline Form-->
  <div class="col-lg-8">                           
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Cari Buku</h3>
      </div>
      <div class="card-body">
        <form class="form-inline" action="{{ URL('pinjam') }}">
          <div class="form-group">
            <label for="inlineFormInput" class="sr-only">Name</label>
            <input id="inlineFormInput" name="nama_buku" type="text" placeholder="Judul Buku" class="mr-3 form-control">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Cari</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="card">
      <div class="card-close">
        <div class="dropdown">
          <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
          <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
        </div>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4">Daftar Buku</h3>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Buku</th>
                <th>Kode Buku</th>
                <th>Penerbit</th>
                <th>Jurusan</th>
                <th>Jenis Buku</th>
                <th>Tersedia</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Kalkulus 1</td>
                <td>TB-001-K</td>
                <td>Erlangga</td>
                <td>Informatika</td>
                <td>Text Book</td>
                <td>-</td>
                <td class="text-center">
                  <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Detail</button>
                  <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Buku Habis">
                    <button style="pointer-events: none;" class="btn btn-primary" action="{{ URL('form_pinjam') }}" disabled>Pinjam</button>
                  </span>
                  <!-- <a href="{{ URL('form_pinjam') }}" class="btn btn-primary disabled">Pinjam</a> -->
                </td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Dasar Pemrograman</td>
                <td>TB-010-DP</td>
                <td>Erlangga</td>
                <td>Informatika</td>
                <td>Text Book</td>
                <td>2</td>
                <td class="text-center">
                  <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Detail</button>
                  <a href="{{ URL('form_pinjam') }}" class="btn btn-primary">Pinjam</a>
                </td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>Pengantar Teknologi</td>
                <td>M-011-PT</td>
                <td>Erlangga</td>
                <td>Informatika</td>
                <td>Makalah</td>
                <td>-</td>
                <td class="text-center">
                  <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Detail</button>
                  <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Buku Tidak Boleh Dipinjam">
                    <button style="pointer-events: none;" class="btn btn-primary" action="{{ URL('form_pinjam') }}" disabled>Pinjam</button>
                  </span>
                </td>
              </tr>
              <tr>
                <th scope="row">4</th>
                <td>Deep Learning</td>
                <td>TA-110-DL</td>
                <td>Erlangga</td>
                <td>Teknik Komputer</td>
                <td>Tugas Akhir</td>
                <td>2</td>
                <td class="text-center">
                  <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Detail</button>
                 <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Buku Tidak Boleh Dipinjam">
                    <button style="pointer-events: none;" class="btn btn-primary" action="{{ URL('form_pinjam') }}" disabled>Pinjam</button>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Form-->
  <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 id="exampleModalLabel" class="modal-title">Detail Buku</h4>
          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-6" style="margin-top: 5%;">
              <img src="{{asset('/gambar/kalkulus.jpg')}}" height="400">
            </div>
            <div class="col-6">
              <form>
                <div class="form-group">
                  <label>Nama Buku</label>
                  <textarea rows="1" class="form-control" disabled>Kalkulus</textarea>
                </div>
                <div class="form-group">
                  <label>Penerbit</label>
                  <textarea rows="1" class="form-control" disabled>Erlangga</textarea>
                </div>
                <div class="form-group">       
                  <label>Lokasi</label>
                  <textarea rows="1" class="form-control" disabled>Informatika</textarea>
                </div>
                <div class="form-group">       
                  <label>Status</label>
                  <textarea rows="1" class="form-control" disabled>Boleh Dipinjam</textarea>
                </div>
                <div class="form-group">       
                  <label>Detail</label>
                  <textarea rows="4" class="form-control" disabled>cabang ilmu matematika yang mencakup limit, turunan, integral, dan deret takterhingga</textarea>
                </div>
              </form>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
          <a href="{{ URL('form_pinjam') }}" class="btn btn-primary">Pinjam</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
