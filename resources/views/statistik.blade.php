@extends('master')
@section('slide2')
active
@endsection
@section('content')
<div class="row">
  <div class="chart col-lg-12 col-12">
    <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
      <canvas id="lineCahrt"></canvas>
    </div>
  </div>  
</div>

@endsection
