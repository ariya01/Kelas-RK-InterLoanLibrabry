@extends('master')
@section('slide7')
active
@endsection
@section('content')
<div class="row">
                
                
                 <div class="col-lg-4">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Tambah Akun</h3>
                    </div>
                    <div class="card-body">
                       <button data-toggle="modal" data-target="#myModal1" class="btn btn-primary">Tambah</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Daftar Akun</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Jurusan</th>
                              <th>Status</th>
                              <th class="text-center">Option</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Muhammad Didin Anyar</td>
                              <td>Informatika</td>
                              <td>Pustakawan</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Edit</button>
                                <button data-toggle="modal" data-target="" class="btn btn-danger">Hapus</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Chaya Putra Hikmawan</td>
                              <td>Informatika</td>
                              <td>Mahasiswa</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Edit</button>
                                <button data-toggle="modal" data-target="" class="btn btn-danger">Hapus</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>Himayan Wijaya</td>
                              <td>Informatika</td>
                              <td>Mahasiswa</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Edit</button>
                                <button data-toggle="modal" data-target="" class="btn btn-danger">Hapus</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Modal Form-->
                <div id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">TAMBAH AKUN</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label>Nama Akun</label>
                                  <input type="email" value="Daniel" class="form-control" >
                                </div>
                                <div class="form-group">
                                  <label>No. Identitas</label>
                                  <input type="email" value="5115100022" class="form-control" >
                                </div>
                                <div class="form-group">       
                                  <label>Jurusan</label>
                                  <input type="text" value="Informatika" class="form-control" >
                                </div>
                                <div class="form-group ">
                                  <label>Status</label>
                                   <div class="dropdown">
                                        <div class="form-group">
                                          <select class="form-control">
                                            {{-- <option>Pilih Login</option> --}}
                                            <option>Mahasiswa</option>
                                            <option>Dosen</option>
                                            <option>Karyawan/Tendik</option>
                                            <option>Umum</option>
                                            <option>Pustakawan</option>
                                          </select>
                                        </div>
                                  </div>            
                                </div>
                                <div class="form-group">       
                                  <label>Alamat</label>
                                  <input type="text" value="Jl.Sutorejo Selatan III" class="form-control" >
                                </div>
                                <div class="form-group">       
                                  <label>No Telepon</label>
                                  <input type="text" value="082789009900" class="form-control" >
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              <button type="button" class="btn btn-primary">Tambah</button>
                            </div>
                          </div>
                        </div>
                      </div>

                <!-- Modal Form-->
                <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">Edit Profil</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label>Nama Akun</label>
                                  <input type="email" value="Daniel" class="form-control" >
                                </div>
                                <div class="form-group">
                                  <label>No. Identitas</label>
                                  <input type="email" value="5115100022" class="form-control" >
                                </div>
                                <div class="form-group">       
                                  <label>Jurusan</label>
                                  <input type="text" value="Informatika" class="form-control" >
                                </div>
                                <div class="form-group ">
                                  <label>Status</label>
                                   <div class="dropdown">
                                        <div class="form-group">
                                          <select class="form-control">
                                            {{-- <option>Pilih Login</option> --}}
                                            <option>Mahasiswa</option>
                                            <option>Dosen</option>
                                            <option>Karyawan/Tendik</option>
                                            <option>Umum</option>
                                            <option>Pustakawan</option>
                                          </select>
                                        </div>
                                  </div>            
                                </div>
                                <div class="form-group">       
                                  <label>Alamat</label>
                                  <input type="text" value="Jl.Sutorejo Selatan III" class="form-control" >
                                </div>
                                <div class="form-group">       
                                  <label>No Telepon</label>
                                  <input type="text" value="082789009900" class="form-control" >
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              <button type="button" class="btn btn-primary">Tambah</button>
                            </div>
                          </div>
                        </div>
                      </div>
              </div>

@endsection
