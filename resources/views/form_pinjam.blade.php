@extends('master_user')
@section('slide4')
active
@endsection
@section('content')
              <!-- Forms Section-->
            <div class="container-fluid">
              <div class="row">
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Form Peminjaman</h3>
                    </div>
                    <div class="card-body">
                      <form class="form-horizontal">
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Nama</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="Findryan Kurnia P." disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">No. ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="5115100035" disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Tanggal Peminjaman</label>
                          <div class="col-sm-9">
                            <input type="date" name="password" class="form-control" value="" >
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Batas Pengembalian</label>
                          <div class="col-sm-9">
                            
                            <input type="date" name="password" class="form-control" value="" >
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Nama Buku</label>
                          <div class="col-sm-9">
                            <input type="text" name="password" class="form-control" value="Aljabar Linear" disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Tempat Peminjaman</label>
                          <div class="col-sm-9">
                            <input type="text" name="password" class="form-control" value="Informatika" disabled>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Persetujuan<br><small class="text-primary">*Harap di centang untuk dapat melakukan peminjaman</small></label>
                          <div class="col-sm-9">
                              <div class="i-checks">
                              <input id="checkboxCustom1" type="checkbox" value="" class="checkbox-template">
                              <label for="checkboxCustom1"  style="font-size: 16px;">Saya bersedia untuk memenuhi segala peraturan yang berlaku</label>
                              <button type="button" class= "btn btn-danger" data-toggle="modal" data-target="#myModal2"> Peraturan </button>
                            </div>
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                            <button type="submit" class="btn btn-secondary">Cancel</button>
                            <a href="{{ URL('pinjam') }}" class="btn btn-primary">Pinjam</a>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {{-- //modal --}}

            <div id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">PERATURAN</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <ol>
                                  1. Batas Lama Peminjaman untuk sekali peminjaman buku selama 7 Hari
                                </ol>
                                <ol>
                                  2. Biaya denda untuk setiap hari keterlambatan sebagai berikut : Rp.5000/Hari 
                                </ol>
                              </ul>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              <a href="{{ URL('form_pinjam') }}" class="btn btn-primary">Pinjam</a>
                            </div>
                          </div>
                        </div>
                      </div>
@endsection
