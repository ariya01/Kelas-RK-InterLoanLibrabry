@extends('master')
@section('slide3')
active
@endsection
@section('content')
<div class="row">
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Filter Transaksi Peminjaman</h3>
                    </div>
                    <div class="card-body">
                      <form class="form-inline">
                        <div class="form-group text-center">
                          <select id="inlineFormInput" name="account" class="btn btn-primary">
                              <option>Pilih Jurusan</option>
                              <option>Informatika</option>
                              <option>Matematika</option>
                              <option>Statistika</option>
                            </select>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Daftar Transaksi</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Id Transaksi</th>
                              <th>Nama Pegawai</th>
                              <th>Nama Peminjam</th>
                              <th>Nama Buku</th>
                              <th>Jurusan</th>
                              <th>Tanggal Peminjaman</th>
                              <th>Tanggal Pengembalian</th>
                              <th class="text-center">Option</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>00001</td>
                              <td>Muhammad Didin Anyar  </td>
                              <td>Findryan Kurnia</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td>02-02-2018</td>
                              <td>12-02-2018</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Denda</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>00002</td>
                              <td>Muhammad Didin Anyar  </td>
                              <td>Findryan Kurnia</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td>02-02-2018</td>
                              <td>12-02-2018</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Denda</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>00003</td>
                              <td>Muhammad Didin Anyar  </td>
                              <td>Findryan Kurnia</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td>02-02-2018</td>
                              <td>12-02-2018</td>
                              <td class="text-center">
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Denda</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Modal Form-->
                <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">DENDA</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                              <p>ID Transaksi : 000001</p>
                              <form>
                                <div class="form-group">
                                  <label>Nama Buku</label>
                                  <input type="email" value="Aljabar Linear" class="form-control" disabled>
                                </div>
                                <div class="form-group">       
                                  <label>Jumlah</label>
                                  <input type="text" value="Rp. 5000" class="form-control" disabled>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
              </div>
@endsection
