@extends('master')
@section('slide9')
active
@endsection
@section('content')
                            <div class="row">
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard3" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Filter Calon Peminjaman</h3>
                    </div>
                    <div class="card-body">
                      <form class="form-inline">
                        <div class="form-group text-center">
                          <select id="inlineFormInput" name="account" class="btn btn-primary">
                              <option>Pilih Jurusan</option>
                              <option>Informatika</option>
                              <option>Matematika</option>
                              <option>Statistika</option>
                            </select>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Daftar Calon Peminjam</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Id Transaksi</th>
                              <th>Nama Buku</th>
                              <th>Jurusan</th>
                              <th class="text-center">Option</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>00201</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td class="text-center">
                                <button data-toggle="modal" class="btn btn-primary">Terima</button>
                                <button data-toggle="modal" class="btn btn-danger">Tolak</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>00001</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td class="text-center">
                                <button data-toggle="modal" class="btn btn-primary">Terima</button>
                                <button data-toggle="modal" class="btn btn-danger">Tolak</button>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>00011</td>
                              <td>Matematika Distrik</td>
                              <td>Informatika</td>
                              <td class="text-center">
                                <button data-toggle="modal" class="btn btn-primary">Terima</button>
                                <button data-toggle="modal" class="btn btn-danger">Tolak</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
@endsection
