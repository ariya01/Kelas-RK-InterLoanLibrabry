<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class MyControll extends Controller
{
    //
    public function login()
    {
    	return view('login');
    }
    public function login_akun(Request $request)
    {
        $nama = $request->nama;
        $pass = $request->password;
        if ($nama == '5115100035')
        {
            return view('pinjam');
        }
        else if ($nama == '22001')
        {
            return view('home_pegawai');
        }
        else
        {
            return view('login');
        }
    }
    public function home()
    {
    	return view('home_pegawai');
    }
    public function statistik()
    {
    	return view('statistik');
    }
    public function transaksi()
    {
    	return view('transaksi');
    }
    public function kelola()
    {
    	$data = DB::table('buku')->get();
        // dd($data);
        return view('kelola',compact('data'));
    }
    public function akun()
    {
        return view ('akun');
    }
    public function verif()
    {
        return view ('verif');
    }
    public function konfirmasi_peminjaman()
    {
        return view('konfir');
    }
    public function peraturan_perpustakaan()
    {
        return view('peraturan');
    }

    //PEMINJAM
    public function home_user()
    {
        return view('pinjam');
    }
    public function pinjam()
    {
        return view('pinjam');
    }
    public function form_pinjam()
    {
        return view('form_pinjam');
    }
    public function denda()
    {
        return view('denda');
    }
    public function inputbuku(Request $request)
    {
        $file=$request->file('gambar');
        $file->move(base_path('public\images'),time().'.jpg');
        // dd($path);
        $data = DB::table('buku')->insert(['nama_buku'=>$request->nama_buku,'deskripsi'=>$request->deskripsi,'jurusan'=>$request->jurusan,'tersedia'=>$request->tersedia,'jenis_buku'=>$request->jenis_buku,'kode_buku'=>$request->kode_buku,'gambar'=>time().'.jpg','penerbit'=>$request->penerbit]);
        return redirect()->route('kelola');
    }
    public function hapusbuku(Request $request)
    {
        $data = DB::table('buku')->where('id_buku','=',$request->id_buku)->delete();
        return redirect()->route('kelola');
    }
    public function editbuku(Request $request)
    {
        $file=$request->file('gambar');
        $file->move(base_path('public\images'),time().'.jpg');
        $data = DB::table('buku')->where('id_buku','=',$request->id_buku)->update(['nama_buku'=>$request->nama_buku,'deskripsi'=>$request->deskripsi,'jenis_buku'=>$request->jenis_buku,'kode_buku'=>$request->kode_buku,'jurusan'=>$request->jurusan,'tersedia'=>$request->tersedia,'gambar'=>time().'.jpg','penerbit'=>$request->penerbit]);
        return redirect()->route('kelola');
    }
}
