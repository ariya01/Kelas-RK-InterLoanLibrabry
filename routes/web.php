<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//pegawai
Route::get('/','MyControll@login');
Route::get('home_pegawai','MyControll@home');
Route::get('home','MyControll@home');
Route::get('home_user','MyControll@home_user');
Route::get('statistik','MyControll@statistik');
Route::get('transaksi','MyControll@transaksi');
Route::get('kelola','MyControll@kelola')->name('kelola');
Route::get('akun','MyControll@akun');
Route::get('verif','MyControll@verif');
Route::get('peraturan','MyControll@peraturan_perpustakaan');
Route::get('konfirmasi_peminjaman','MyControll@konfirmasi_peminjaman');
Route::post('login_akun','MyControll@login_akun');
Route::post('/inputbuku','MyControll@inputbuku');
Route::post('/hapusbuku','MyControll@hapusbuku');
Route::post('/editbuku','MyControll@editbuku');

//peminjam
Route::get('pinjam','MyControll@pinjam');
Route::get('form_pinjam','MyControll@form_pinjam');
Route::get('denda','MyControll@denda');