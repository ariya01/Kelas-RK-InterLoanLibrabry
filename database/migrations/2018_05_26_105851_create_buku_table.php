<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->increments('id_buku');
            $table->string('nama_buku');
            $table->string('deskripsi');
            $table->string('gambar');
            $table->string('jenis_buku');
            $table->string('kode_buku');
            $table->integer('jurusan')->unsigned();
            $table->integer('penerbit')->unsigned();
            $table->integer('tersedia');
            $table->foreign('jurusan')
            ->references('id_jurusan')->on('jurusan')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('penerbit')
            ->references('id_penerbit')->on('penerbit')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
